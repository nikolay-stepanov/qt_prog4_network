#include "WeatherData.h"

WeatherData::WeatherData(QObject *parent) : QObject(parent)
{

}

WeatherData::WeatherData(const WeatherData &other):
    QObject(0),
    m_dayOfWeek(other.m_dayOfWeek),
    m_weatherIcon(other.m_weatherIcon),
    m_weatherDescription(other.m_weatherDescription),
    m_temperature(other.m_temperature)
{

}

QString WeatherData::dayOfWeek() const
{
    return m_dayOfWeek;
}

void WeatherData::setDayOfWeek(const QString &dayOfWeek)
{
    m_dayOfWeek = dayOfWeek;
    emit dataChanged();
}

QString WeatherData::weatherIcon() const
{
    return m_weatherIcon;
}

void WeatherData::setWeatherIcon(const QString &weather)
{
    m_weatherIcon = weather;
    emit dataChanged();
}

QString WeatherData::weatherDescription() const
{
    return m_weatherDescription;
}

void WeatherData::setWeatherDescription(const QString &weatherDescription)
{
    m_weatherDescription = weatherDescription;
    emit dataChanged();
}

QString WeatherData::temperature() const
{
    return m_temperature;
}

void WeatherData::setTemperature(const QString &temperature)
{
    m_temperature = temperature;
    emit dataChanged();
}
