import QtQuick 2.12
import QtQuick.Window 2.12

import WeatherInfo 1.0

Window {
    id: window
    objectName: "window"
    visible: true
    width: 1000
    height: 800
    color: "#42aaff"
    title: qsTr("Weather")


    Item{
        id:stateWindow

        anchors.fill: parent

        state: "loading"

        states: [
            State {
                name: "loading"
                PropertyChanges { target: main; opacity: 0 }
                PropertyChanges { target: wait; opacity: 1 }
            },
            State {
                name: "ready"
                PropertyChanges { target: main; opacity: 1 }
                PropertyChanges { target: wait; opacity: 0 }
            }
        ]
        WeatherAppModel {
            id: model
            onReadyChanged: {
                if (model.ready)
                    stateWindow.state = "ready"
                else
                    stateWindow.state = "loading"
            }
        }
        Item {
            id: wait
            anchors.fill: parent

            Text {
                text: "Loading weather data..."
                anchors.centerIn: parent
                font.pointSize: 18
            }
        }
    }

    Item{
        id: main
        anchors.fill: parent
        Column {
            spacing: 6
            anchors {
                fill: parent
                topMargin: 20; bottomMargin: 20; leftMargin: 20; rightMargin: 20
            }

            Rectangle {
                width: parent.width
                height: 25
                color: "#1b97f5"

                Text {
                    text: (model.hasValidCity ? model.city : "Unknown location")
                    anchors.fill: parent
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                }

                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        switch (model.city) {
                        case "Nizhny Novgorod":
                            model.city = "Moscow"
                            break
                        case "Moscow":
                            model.city = "Saint Petersburg"
                            break
                        case "Saint Petersburg":
                            model.city = "Kazan"
                            break
                        case "Kazan":
                            model.city = "Nizhny Novgorod"
                            break;
                        }
                    }
                }
            }


            MainWeatherForecastIcon {
                id: mainWeatherForecastIcon

                width: main.width - 20
                height: 2 * (main.height -25-20) / 3

                weatherIcon:(model.hasValidWeather
                             ? model.weather.weatherIcon
                             : "01d")
                topText:(model.hasValidWeather
                         ? model.weather.temperature
                         : "??")
                bottomText: (model.hasValidWeather
                             ? model.weather.weatherDescription
                             : "No weather data")
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        model.refreshWeather()
                    }
                }
            }

            Row {
                id: iconRow
                spacing: 5

                width: main.width - 20
                height: (main.height - 25 - 40) / 3

                property real iconWidth: iconRow.width / 5 - 10
                property real iconHeight: iconRow.height

                WeatherForecastIcon
                {
                    id: forecast1
                    width: iconRow.iconWidth
                    height: iconRow.iconHeight

                    topText:(model.hasValidWeather ?
                                 model.forecast[0].dayOfWeek : "??")
                    bottomText:(model.hasValidWeather ?
                                    model.forecast[0].temperature : "??/??")
                    weatherIcon: (model.hasValidWeather ?
                                      model.forecast[0].weatherIcon : "01d")
                }

                WeatherForecastIcon
                {
                    id: forecast2
                    width: iconRow.iconWidth
                    height: iconRow.iconHeight

                    topText:(model.hasValidWeather ?
                                 model.forecast[1].dayOfWeek : "??")
                    bottomText:(model.hasValidWeather ?
                                    model.forecast[1].temperature : "??/??")
                    weatherIcon: (model.hasValidWeather ?
                                      model.forecast[1].weatherIcon : "01d")
                }

                WeatherForecastIcon
                {
                    id: forecast3
                    width: iconRow.iconWidth
                    height: iconRow.iconHeight

                    topText: (model.hasValidWeather ?
                                  model.forecast[2].dayOfWeek : "??")
                    bottomText: (model.hasValidWeather ?
                                     model.forecast[2].temperature : "??/??")
                    weatherIcon: (model.hasValidWeather ?
                                      model.forecast[2].weatherIcon : "01d")
                }

                WeatherForecastIcon
                {
                    id: forecast4
                    width: iconRow.iconWidth
                    height: iconRow.iconHeight

                    topText: (model.hasValidWeather ?
                                  model.forecast[3].dayOfWeek : "??")
                    bottomText: (model.hasValidWeather ?
                                     model.forecast[3].temperature : "??/??")
                    weatherIcon: (model.hasValidWeather ?
                                      model.forecast[3].weatherIcon : "01d")
                }

                WeatherForecastIcon
                {
                    id: forecast5
                    width: iconRow.iconWidth
                    height: iconRow.iconHeight

                    topText: (model.hasValidWeather ?
                                  model.forecast[4].dayOfWeek : "??")
                    bottomText: (model.hasValidWeather ?
                                     model.forecast[4].temperature : "??/??")
                    weatherIcon: (model.hasValidWeather ?
                                      model.forecast[4].weatherIcon : "01d")
                }
            }
        }
    }
}
