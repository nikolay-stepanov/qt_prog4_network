import QtQuick 2.0

Item {
    id: root

    property string weatherIcon: "no"

    Image {
        id: img
        source: {
            switch (weatherIcon) {
            case "01d":
                "../icons/day-clear-sky.png"
                break;
            case "01n":
                "../icons/night-clear-sky.png"
                break;
            case "02d":
                "../icons/day-few-clouds.png"
                break;
            case "02n":
                "../icons/night-few-clouds.png"
                break;
            case "03d":
                "../icons/day-scattered-clouds.png"
                break;
            case "03n":
                "../icons/night-scattered-clouds.png"
                break;
            case "04d":
                "../icons/day-broken-clouds.png"
                break;
            case "04n":
                "../icons/night-broken-clouds.png"
                break;
            case "09d":
                "../icons/day-shower-rain.png"
                break;
            case "09n":
                "../icons/night-shower-rain.png"
                break;
            case "10d":
                "../icons/day-rain.png"
                break;
            case "10n":
                "../icons/night-rain.png"
                break;
            case "11d":
                "../icons/day-thunderstorm.png"
                break;
            case "11n":
                "../icons/night-thunderstorm.png"
                break;
            case "13d":
                "../icons/day-snow.png"
                break;
            case "13n":
                "../icons/night-snow.png"
                break;
            case "50d":
                "../icons/day-mist.png"
                break;
            case "50n":
                "../icons/night-mist.png"
                break;
            default:
                "../icons/na.png"
            }
        }
        smooth: true
        anchors.fill: parent
    }

}
