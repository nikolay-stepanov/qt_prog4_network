#ifndef WEATHERAPPMODEL_H
#define WEATHERAPPMODEL_H

#include <WeatherModelPrivate.h>

#include <QObject>

class WeatherAppModel : public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool ready
               READ ready
               NOTIFY readyChanged)
    Q_PROPERTY(bool hasValidCity
               READ hasValidCity
               NOTIFY cityChanged)
    Q_PROPERTY(bool hasValidWeather
               READ hasValidWeather
               NOTIFY weatherChanged)
    Q_PROPERTY(QString city
               READ city WRITE setCity
               NOTIFY cityChanged)
    Q_PROPERTY(WeatherData *weather
               READ weather
               NOTIFY weatherChanged)
    Q_PROPERTY(QQmlListProperty<WeatherData> forecast
               READ forecast
               NOTIFY weatherChanged)
public:
    explicit WeatherAppModel(QObject *parent = nullptr);
     ~WeatherAppModel();

    bool ready() const;
    bool hasValidCity() const;
    bool hasValidWeather() const;
    void hadError(bool tryAgain);

    QString city() const;
    void setCity(const QString &value);

    WeatherData *weather() const;
    QQmlListProperty<WeatherData> forecast() const;

public slots:
    Q_INVOKABLE void refreshWeather();

private slots:
    void networkSessionOpened();
    void handleWeatherNetworkData(QNetworkReply *networkReply);
    void handleForecastNetworkData(QNetworkReply *networkReply);

signals:
    void readyChanged();
    void cityChanged();
    void weatherChanged();


private:
    WeatherModelPrivate * m_modelData;
};

#endif // WEATHERAPPMODEL_H
