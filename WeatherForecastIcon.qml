import QtQuick 2.0

Item {
    id: root

    property string topText: "No"
    property string weatherIcon: "No"
    property string bottomText: "??/??"

    Text {
        id: dayText
        horizontalAlignment: Text.AlignHCenter
        width: root.width
        text: root.topText

        anchors.top: parent.root
        anchors.topMargin: root.height / 5 - dayText.paintedHeight
        anchors.horizontalCenter: parent.horizontalCenter
    }

    WeatherIcon {
        id: icon
        weatherIcon: root.weatherIcon

        property real side: {
            var h = 3 * root.height / 5
            if (root.width < h)
                root.width;
            else
                h;
        }

        width: icon.side
        height: icon.side

        anchors.centerIn: parent
    }

    Text {
        id: tempText
        horizontalAlignment: Text.AlignHCenter
        width: root.width
        text: root.bottomText

        anchors.bottom: parent.bottom
        anchors.bottomMargin: root.height / 5 - tempText.paintedHeight
        anchors.horizontalCenter: parent.horizontalCenter
    }
}
