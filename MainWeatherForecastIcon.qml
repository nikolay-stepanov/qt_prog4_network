import QtQuick 2.0

Item {
    id: root

    property string topText: "no"
    property string bottomText: "No weather description"
    property string weatherIcon: "no"
    property real smallSide: (root.width < root.height ? root.width : root.height)

    Text {
        text: root.topText
        font.bold: true
        font.pointSize: 28
        color: "white"
        anchors {
            top: root.top
            left: root.left
            topMargin: 10
            leftMargin: 10
        }
    }

    WeatherIcon {
        weatherIcon: root.weatherIcon
        anchors.centerIn: parent
        anchors.verticalCenterOffset: -15
        width: root.smallSide
        height: root.smallSide
    }

    Text {
        text: root.bottomText
        font.family: "Segoe UI Black"
        font.pointSize: 24
        color: "white"
        wrapMode: Text.WordWrap
        width: parent.width
        horizontalAlignment: Text.AlignHCenter
        anchors {
            bottom: root.bottom
            right: root.right
            rightMargin: 5
        }
    }

}
