#ifndef WEATHERMODELPRIVATE_H
#define WEATHERMODELPRIVATE_H

#include <QString>
#include <QNetworkAccessManager>
#include <QNetworkSession>
#include <QQmlListProperty>
#include <QTimer>
#include <QElapsedTimer>

#include <WeatherData.h>

class WeatherModelPrivate
{
public:
    static const int baseMsBeforeNewRequest = 5 * 1000;
    QString longitude, latitude;
    QString city;
    QNetworkAccessManager *netAccessManager;
    QNetworkSession *netSession;
    WeatherData nowWeather;
    QList<WeatherData*> forecast;
    QQmlListProperty<WeatherData> *forecastProperty;
    bool ready;
    QElapsedTimer throttle;
    int nErrors;
    int minMsBeforeNewRequest;
    QTimer delayedCityRequestTimer;
    QTimer requestNewWeatherTimer;
    QString app_ident;

    WeatherModelPrivate() :
        netAccessManager(NULL),
        netSession(NULL),
        forecastProperty(NULL),
        ready(false),
        nErrors(0),
        minMsBeforeNewRequest(baseMsBeforeNewRequest)
    {
        delayedCityRequestTimer.setSingleShot(true);
        delayedCityRequestTimer.setInterval(1000); // 1 s
        requestNewWeatherTimer.setSingleShot(false);
        requestNewWeatherTimer.setInterval(10*60*1000); // 10 min
        throttle.invalidate();
        app_ident = QStringLiteral("36496bad1955bf3365448965a42b9eac");
    }
};
#endif // WEATHERMODELPRIVATE_H
