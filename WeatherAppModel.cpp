#include "WeatherAppModel.h"

#include <QDebug>

#include <QUrlQuery>
#include <QNetworkReply>
#include <QNetworkConfigurationManager>

#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>

#define ZERO_KELVIN 273.15

static QString niceTemperatureString(double t)
{
    return QString::number(qRound(t-ZERO_KELVIN)) + QChar(0xB0);
}

static void forecastAppend(QQmlListProperty<WeatherData> *prop, WeatherData *val)
{
    Q_UNUSED(val);
    Q_UNUSED(prop);
}

static WeatherData *forecastAt(QQmlListProperty<WeatherData> *prop, int index)
{
    WeatherModelPrivate *d = static_cast<WeatherModelPrivate*>(prop->data);
    return d->forecast.at(index);
}

static int forecastCount(QQmlListProperty<WeatherData> *prop)
{
    WeatherModelPrivate *d = static_cast<WeatherModelPrivate*>(prop->data);
    return d->forecast.size();
}

static void forecastClear(QQmlListProperty<WeatherData> *prop)
{
    static_cast<WeatherModelPrivate*>(prop->data)->forecast.clear();
}

WeatherAppModel::WeatherAppModel(QObject *parent) :
    QObject(parent),
    m_modelData(new WeatherModelPrivate)
{
    m_modelData->forecastProperty= new QQmlListProperty<WeatherData>(this, m_modelData,
                                                          forecastAppend,
                                                          forecastCount,
                                                          forecastAt,
                                                          forecastClear);

    connect(&m_modelData->requestNewWeatherTimer, SIGNAL(timeout()),
            this, SLOT(refreshWeather()));
    m_modelData->requestNewWeatherTimer.start();

    m_modelData->netAccessManager = new QNetworkAccessManager(this);

    QNetworkConfigurationManager ncm;
    m_modelData->netSession = new QNetworkSession(ncm.defaultConfiguration(), this);
    connect(m_modelData->netSession, SIGNAL(opened()), this, SLOT(networkSessionOpened()));

    if (m_modelData->netSession->isOpen())
        this->networkSessionOpened();

    m_modelData->netSession->open();
}

WeatherAppModel::~WeatherAppModel()
{
    m_modelData->netSession->close();
    delete m_modelData;
}

bool WeatherAppModel::ready() const
{
    return m_modelData->ready;
}

bool WeatherAppModel::hasValidCity() const
{
    return (!(m_modelData->city.isEmpty()) && m_modelData->city.size() > 1 && m_modelData->city != "");
}

bool WeatherAppModel::hasValidWeather() const
{
    return hasValidCity() && (!(m_modelData->nowWeather.weatherIcon().isEmpty()) &&
                              (m_modelData->nowWeather.weatherIcon().size() > 1) &&
                              m_modelData->nowWeather.weatherIcon() != "");
}

void WeatherAppModel::hadError(bool tryAgain)
{
    m_modelData->throttle.start();
    if (m_modelData->nErrors < 10)
        (m_modelData->nErrors)++;
    m_modelData->minMsBeforeNewRequest = (m_modelData->nErrors + 1) * m_modelData->baseMsBeforeNewRequest;
    if (tryAgain)
        m_modelData->delayedCityRequestTimer.start();
}

QString WeatherAppModel::city() const
{
    return m_modelData->city;
}

void WeatherAppModel::setCity(const QString &value)
{
    m_modelData->city = value;
    emit cityChanged();
    refreshWeather();
}

WeatherData *WeatherAppModel::weather() const
{
    return &(m_modelData->nowWeather);
}

QQmlListProperty<WeatherData> WeatherAppModel::forecast() const
{
    return *(m_modelData->forecastProperty);
}

void WeatherAppModel::refreshWeather()
{
    if (m_modelData->city.isEmpty()) {
       qDebug() << "refreshing weather skipped (no city)";
        return;
    }
    qDebug() << "refreshing weather";
    QUrl url("http://api.openweathermap.org/data/2.5/weather");
    QUrlQuery query;

    query.addQueryItem("q", m_modelData->city);
    query.addQueryItem("mode", "json");
    query.addQueryItem("APPID", m_modelData->app_ident);
    url.setQuery(query);

    QNetworkReply *rep = m_modelData->netAccessManager->get(QNetworkRequest(url));

    connect(rep, &QNetworkReply::finished,
            this, [this, rep]() { handleWeatherNetworkData(rep); });
}


void WeatherAppModel::networkSessionOpened()
{
    m_modelData->city = "Nizhny Novgorod";
    emit cityChanged();
    this->refreshWeather();
}

void WeatherAppModel::handleWeatherNetworkData(QNetworkReply *networkReply)
{
    if (!networkReply)
        return;

    if (!networkReply->error()) {
        foreach (WeatherData *inf, m_modelData->forecast)
            delete inf;
        m_modelData->forecast.clear();

        QJsonDocument document = QJsonDocument::fromJson(networkReply->readAll());

        if (document.isObject()) {
            QJsonObject obj = document.object();
            QJsonObject tempObject;
            QJsonValue val;

            if (obj.contains(QStringLiteral("weather"))) {
                val = obj.value(QStringLiteral("weather"));
                QJsonArray weatherArray = val.toArray();
                val = weatherArray.at(0);
                tempObject = val.toObject();
                m_modelData->nowWeather.setWeatherDescription(tempObject.value(QStringLiteral("description")).toString());
                m_modelData->nowWeather.setWeatherIcon(tempObject.value("icon").toString());
            }
            if (obj.contains(QStringLiteral("main"))) {
                val = obj.value(QStringLiteral("main"));
                tempObject = val.toObject();
                val = tempObject.value(QStringLiteral("temp"));
                m_modelData->nowWeather.setTemperature(niceTemperatureString(val.toDouble()));
            }
        }
    }
    networkReply->deleteLater();

    //retrieve the forecast

    QUrl url("http://api.openweathermap.org/data/2.5/forecast/daily");
    QUrlQuery query;

    query.addQueryItem("q", m_modelData->city);
    query.addQueryItem("mode", "json");
    query.addQueryItem("cnt", "6");
    query.addQueryItem("APPID", m_modelData->app_ident);
    url.setQuery(query);

    QNetworkReply *rep = m_modelData->netAccessManager->get(QNetworkRequest(url));
    // connect up the signal right away
    connect(rep, &QNetworkReply::finished,
            this, [this, rep]() { handleForecastNetworkData(rep); });
}

void WeatherAppModel::handleForecastNetworkData(QNetworkReply *networkReply)
{
    if (!networkReply)
        return;

    if (!networkReply->error()) {
        QJsonDocument document = QJsonDocument::fromJson(networkReply->readAll());

        QJsonObject jo;
        QJsonValue jv;
        QJsonObject root = document.object();
        jv = root.value(QStringLiteral("list"));
        if (!jv.isArray())
            qWarning() << "Invalid forecast ";
        QJsonArray ja = jv.toArray();
        if (ja.count() != 6)
            qWarning() << "Invalid forecast object";

        QString data;
        for (int i = 1; i<ja.count(); i++) {
            WeatherData *forecastEntry = new WeatherData();

            //min/max temperature
            QJsonObject subtree = ja.at(i).toObject();
            jo = subtree.value(QStringLiteral("temp")).toObject();
            jv = jo.value(QStringLiteral("min"));
            data.clear();
            data += niceTemperatureString(jv.toDouble());
            data += QChar('/');
            jv = jo.value(QStringLiteral("max"));
            data += niceTemperatureString(jv.toDouble());
            forecastEntry->setTemperature(data);

            //get date
            jv = subtree.value(QStringLiteral("dt"));
            QDateTime dt = QDateTime::fromMSecsSinceEpoch((qint64)jv.toDouble()*1000);
            forecastEntry->setDayOfWeek(dt.date().toString(QStringLiteral("ddd")));

            //get icon
            QJsonArray weatherArray = subtree.value(QStringLiteral("weather")).toArray();
            jo = weatherArray.at(0).toObject();
            forecastEntry->setWeatherIcon(jo.value(QStringLiteral("icon")).toString());

            //get description
            forecastEntry->setWeatherDescription(jo.value(QStringLiteral("description")).toString());

            m_modelData->forecast.append(forecastEntry);
        }

        if (!(m_modelData->ready)) {
            m_modelData->ready = true;
            emit readyChanged();
        }

        emit weatherChanged();
    }
    networkReply->deleteLater();
}
