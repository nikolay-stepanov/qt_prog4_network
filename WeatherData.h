#ifndef WEATHERDATA_H
#define WEATHERDATA_H

#include <QObject>

class WeatherData : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString dayOfWeek
               READ dayOfWeek WRITE setDayOfWeek
               NOTIFY dataChanged)
    Q_PROPERTY(QString weatherIcon
               READ weatherIcon WRITE setWeatherIcon
               NOTIFY dataChanged)
    Q_PROPERTY(QString weatherDescription
               READ weatherDescription WRITE setWeatherDescription
               NOTIFY dataChanged)
    Q_PROPERTY(QString temperature
               READ temperature WRITE setTemperature
               NOTIFY dataChanged)

public:
    explicit WeatherData(QObject *parent = nullptr);
     WeatherData(const WeatherData &other);


    QString dayOfWeek() const;
    void setDayOfWeek(const QString &dayOfWeek);

    QString weatherIcon() const;
    void setWeatherIcon(const QString &weatherIcon);

    QString weatherDescription() const;
    void setWeatherDescription(const QString &weatherDescription);

    QString temperature() const;
    void setTemperature(const QString &temperature);

signals:
    void dataChanged();

private:
    QString m_dayOfWeek;
    QString m_weatherIcon;
    QString m_weatherDescription;
    QString m_temperature;
};

Q_DECLARE_METATYPE(WeatherData)

#endif // WEATHERDATA_H
